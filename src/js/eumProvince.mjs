import 'whatwg-fetch';
import './components/fixes.mjs';
import initKlaro from './components/klaro';
import { initTextSize, toggleColorTheme } from './components/pageSettings.mjs';
import newsDashboard from './components/newsDashboard.mjs';
import newsFilters from './components/newsFilters.mjs';
import HistoryCarousel from './components/historyCarousel.mjs';
import DonationBox from './components/donationBox.mjs';
import initSharer from './components/share';

initKlaro();
const klaroTriggers = document.querySelectorAll('.js-klaro-modal-trigger');
if (klaroTriggers) {
  klaroTriggers.forEach(i => {
    i.addEventListener('click', () => window.klaro.show());
  });
}

// Global categories for news filters
(async () => {
  if (typeof window.EUM === 'undefined') { window.EUM = {}; }
  const JSONLinkEl = document.querySelector('link[rel="https://api.w.org/"]');
  const baseUrl = JSONLinkEl ? JSONLinkEl.getAttribute('href') : 'https://gesuiti.it/wp-json/';

  window.EUM.categories = await fetch( `${ baseUrl }wp/v2/categories/?per_page=30` )
    .then(res => (res.ok ? res.json() : new Promise().reject(res)));
})();

// scrolltop
const scrollToTop = new window.jesuitsFramework.ScrollToTop(document.getElementById('scroll-to-top'));
scrollToTop.init();

// eslint-disable-next-line no-undef
Array.from(document.querySelectorAll('.carousel__slider')).forEach(el => new Flickity(el, {
  cellAlign: 'left',
  wrapAround: true,
  imagesLoaded: true,
}));

// News Dashboard
if (document.querySelector('.news-dashboard .js-news-selector')) newsDashboard();

// News Grid Topic Filters
const newsGrids = document.querySelectorAll('.js-news-dynamic-grid');
if (newsGrids) {
  newsGrids.forEach(grid => {
    newsFilters(grid);
  });
}

// Churches
const churchList = document.querySelector('.church-list');

if (churchList) {
  const input = churchList.querySelector('.js-church-list-input');
  const churches = churchList.querySelectorAll('.js-church-item');

  input.addEventListener('change', e => {
    const newCity = e.target.value;
    churches.forEach(church => {
      if (newCity === 'all' || newCity === church.getAttribute('data-city')) {
        church.style.display = '';
      } else {
        church.style.display = 'none';
      }
    });
  });
}

// Media dasboard audio: handle parent element click
const audioWrapper = document.querySelector('.js-media-dashboard-audio');
if (audioWrapper) {
  const audio = audioWrapper.querySelector('audio');
  audioWrapper.addEventListener('click', e => {
    if (e.target !== audio) audio.play();
    return true;
  });
}

const historySection = document.querySelector('.js-history-carousel');
if (historySection) {
  // eslint-disable-next-line no-unused-vars
  const carousel = new HistoryCarousel(historySection);
}

// Faq accordion
const faqSection = document.querySelector('#faq-accordion');
if (faqSection) {
  // eslint-disable-next-line no-undef
  const faqAccordion = new jesuitsFramework.Accordion(faqSection);
  faqAccordion.init();
}

// Donation box
if (document.querySelector('.donation-box')) {
  DonationBox();

  const donationModal = document.querySelector('.js-donation-landing-modal');

  if (donationModal) {
    const modalManager = new window.jesuitsFramework.Modal();
    modalManager.open(donationModal);
  }
}

// Page settings
const textSizeControls = document.querySelectorAll('.text-size-controls input');
textSizeControls.forEach(input => {
  input.addEventListener('click', initTextSize);
});

document.querySelectorAll('.js-color-theme-toggle').forEach(btn => {
  btn.addEventListener('click', toggleColorTheme);
});

// Share functionality
const shareItems = document.querySelectorAll('.js-social-share');
if (shareItems) {
  shareItems.forEach(element => {
    initSharer(element);
  });
}

// Youtube lazyload test (new)
function loadVideo(e) {
  const element = e.target.classList.contains('js-youtube-player') ? e.target : e.target.parentElement;
  const player = element.querySelector('iframe');
  player.src = player.dataset.src;
  player.removeAttribute('data-src');
  element.classList.add('is-loaded');
  element.removeEventListener('click', loadVideo);
}

const videos = document.querySelectorAll('.js-youtube-player');
if (videos) {
  videos.forEach(video => {
    video.parentElement.addEventListener('click', loadVideo);
  });
}
