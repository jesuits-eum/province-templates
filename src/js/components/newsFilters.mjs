/* eslint-disable no-underscore-dangle */
import compileArticleTemplate from './articleTemplate.mjs';

const newsSelector = async element => {
  const store = {};
  const isFeatured = element.classList.contains('js-featured');
  const numberPosts = isFeatured ? 5 : 6;
  const filters = element.querySelector('.js-news-filters');
  const articlesZone = element.querySelector('.js-news-articles');
  const articlesHTMLInitial = articlesZone.innerHTML;
  const columnClassName = articlesZone.querySelector('.col-12:last-child').classList.value;
  const topicLink = element.querySelector('.js-topic-link');

  const JSONLinkEl = document.querySelector('link[rel="https://api.w.org/"]');
  const baseUrl = JSONLinkEl ? JSONLinkEl.getAttribute('href') : 'https://gesuiti.it/wp-json/';

  const compileTemplate = (articles, termID) => {
    const articlesHTML = termID ? articles
      .map((el, i) => {
        let isLarge = false;
        let imgSizes = '(min-width: 1600px) 398px, (min-width: 1200px) 356px, (min-width: 1000px) 438px, (min-width: 760px) 344px, (min-width: 564px) 50vw, (max-width: 563px) 100vw, 356px';
        let col = columnClassName;
        if (isFeatured && i < 2) {
          col = 'col-12 col-s-6';
          isLarge = true;
          imgSizes = '(min-width: 1600px) 692px, (min-width: 1200px) 558px, (min-width: 1000px) 438px, (min-width: 760px) 344px, (min-width: 564px) 50vw, (max-width:563px) 100vw, 356px';
        }
        return `<div class="${col}">${compileArticleTemplate(el, false, isLarge, imgSizes)}</div>`;
      }).join('') : articlesHTMLInitial;
    articlesZone.innerHTML = articlesHTML;
  };

  Array.prototype.slice.call(filters.querySelectorAll('.btn')).forEach(filter => {
    filter.addEventListener('click', async e => {
      if (e.target.classList.contains('is-selected')) return;

      filters.querySelector('.is-selected').classList.remove('is-selected');
      e.target.classList.add('is-selected');

      const termID = parseInt(e.target.dataset.value, 10);

      // Build articles
      if (store[termID] || termID === 0) {
        compileTemplate(store[termID], termID);
      } else {
        element.classList.add('is-loading');
        const tax = (e.target.dataset.tax === 'post_tag') ? 'tags' : 'categories';
        const news = await fetch( `${ baseUrl }wp/v2/posts?per_page=${ numberPosts }&${ tax }=${ termID }&_embed=wp:featuredmedia` )
          .then(res => (res.ok ? res.json() : new Promise().reject(res)));

        if (typeof news !== 'undefined' && news.length > 0) {
          news.map(el => {
            // eslint-disable-next-line prefer-destructuring
            el.thumbnail = Object.prototype.hasOwnProperty.call(el, '_embedded') ? el._embedded['wp:featuredmedia'][0] : null;
            return el;
          });
        }

        store[termID] = news;
        compileTemplate(news, termID);

        element.classList.remove('is-loading');
      }

      // Update news link
      if (topicLink) {
        if (termID === 0) {
          topicLink.style.display = 'none';
          topicLink.querySelector('span').textContent = '';
        } else {
          topicLink.querySelector('span').textContent = e.target.textContent;
          topicLink.href = e.target.dataset.link;
          topicLink.style.display = '';
        }
      }
    });
  });
};

export default newsSelector;
