/**
 * This function detect keyboard or mouse usage
 * for :focus styling purposes, see css class
 */
function detectMouseOrKeyboard() {
  document.addEventListener('mousedown', () => {
    document.body.classList.add('uses-mouse');
  });

  document.addEventListener('keydown', () => {
    document.body.classList.remove('uses-mouse');
  });
}

detectMouseOrKeyboard();

/**
 * Polyfill for forEach on nodelist
 */
if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = Array.prototype.forEach;
}
