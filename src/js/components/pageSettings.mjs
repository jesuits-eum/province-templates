function toggleColorTheme() {
  const root = document.getElementsByTagName('html')[0];
  if (root.classList.contains('is-theme-dark')) {
    root.classList.remove('is-theme-dark');
    localStorage.removeItem('color-theme');
  } else {
    root.classList.add('is-theme-dark');
    if (window.klaro.getManager().getConsent('accessibility') === true) {
      localStorage.setItem('color-theme', 'dark');
    }
  }
}

function initTextSize(e) {
  const root = document.getElementsByTagName('html')[0];
  root.classList.remove('is-text-small');
  root.classList.remove('is-text-default');
  root.classList.remove('is-text-large');
  root.classList.add(`is-text-${e.target.value}`);

  if (window.klaro.getManager().getConsent('accessibility') === true) {
    // Save localStorage key
    window.localStorage.setItem('text-size', e.target.value);
  }

  // Emit resize events for carousels and other dynamically-sized elements
  if (!window.MSInputMethodContext) {
    window.dispatchEvent(new Event('resize'));
  } else {
    const resizeEvent = window.document.createEvent('UIEvents');
    resizeEvent.initUIEvent('resize', true, false, window, 0);
    window.dispatchEvent(resizeEvent);
  }
}

function resetPageSettingsCookies(hasConsent) {
  if (!hasConsent) {
    localStorage.removeItem('text-size');
    localStorage.removeItem('color-theme');
  }
}

export {
  toggleColorTheme,
  initTextSize,
  resetPageSettingsCookies,
};
