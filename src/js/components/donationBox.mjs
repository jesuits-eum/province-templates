const donationBox = () => {
  const main = document.querySelector('.donation-box');

  const buttons = main.querySelectorAll('button[type=button]');
  const donationImport = main.querySelector('input[type=number]');
  const nexiImport = main.querySelector('input[name=importo]');
  const nexiMac = main.querySelector('input[name=mac]');
  const nexiCodTrans = main.querySelector('input[name=codTrans]');

  buttons.forEach(button => button.addEventListener('click', () => {
    donationImport.setAttribute('value', parseInt(button.getAttribute('data-value'), 10));
  }));

  const handleSubmit = e => {
    const { ajaxUrl } = window.eumProv;
    e.preventDefault();
    main.classList.add('is-loading');
    const newImport = donationImport.value * 100;
    const data = new FormData();
    data.append('action', 'eum_donation_data');
    data.append('amount', newImport);

    fetch(ajaxUrl, {
      method: 'POST',
      credentials: 'same-origin',
      body: data,
    })
      .then(response => {
        const resp = response.json();
        // console.log(resp);
        return resp;
      })
      .then(data => {
        nexiCodTrans.value = data.data.codTrans;
        nexiImport.value = data.data.importo;
        nexiMac.value = data.data.mac;
        // Mirror description and note1 for accounting purposes
        main.querySelector('input[name=Note1]').value = main.querySelector('select[name=descrizione]').value;
        main.removeEventListener('submit', handleSubmit);
        main.querySelector('form').submit();
      })
      .catch(error => {
        console.log('[EUM Donations]');
        console.error(error);
        return false;
      });
  };

  if (typeof window.eumProv !== 'undefined') {
    main.addEventListener('submit', handleSubmit);
  }
};

export default donationBox;
