export default function initSharer(element) {

	//review use of btn and markup
  const btn = element.querySelector('.js-native-share-btn');

	if ( btn && navigator.share && window.isSecureContext) {
    element.classList.add('is-native-share');

    let url = document.location.href;
    const canonicalElement = document.querySelector('link[rel=canonical]');
    if (canonicalElement !== null) {
      url = canonicalElement.href;
    }

    element.addEventListener('click', () => {
      navigator.share({
        title: document.title,
        // eslint-disable-next-line object-shorthand
        url: url,
      }).then(() => {
        // eslint-disable-next-line no-console
        console.log('Page succesfully shared using native browser functionality');
      }).catch(console.error);
    });
  } else {
		if ( btn ) {
 			btn.parentElement.removeChild(btn);
		}
	}
}
