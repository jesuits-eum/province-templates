// Once Jesuits Templates will have its own package, this will be in its JS file
import Component from './_Component.mjs';

export default class HistoryCarousel extends Component {
  constructor(element) {
    super(element);
    this.element = element;
    this.slider = this.element.querySelector('.js-history-carousel-slider');
    this.input = this.element.querySelector('.js-history-carousel-input');
    this.ticks = this.element.querySelectorAll('.js-history-carousel-ticks span');
    this.flickity = this.initFlickity();

    this.setEvents();
  }

  initFlickity() {
    // eslint-disable-next-line no-undef
    return new Flickity(this.slider, {
      cellSelector: '.js-history-carousel-slide',
      dragThreshold: 10,
      pageDots: false,

      on: {
        change: index => {
          if (this.input !== document.activeElement) {
            this.input.value = index;
          }
        },
        ready: () => {
          this.moveButtons();
          this.setTicksPosition();
        },
      },
    });
  }

  // Relocate flickity arrow buttons
  moveButtons() {
    const buttons = this.element.querySelectorAll('.flickity-prev-next-button');
    Array.prototype.slice.call(buttons).forEach(el => {
      this.input.parentElement.appendChild(el);
    });
  }

  // Set ticks horizontal positioning
  setTicksPosition() {
    const gutter = 100 / (this.ticks.length - 1);
    Array.prototype.slice.call(this.ticks).forEach((el, index) => {
      if (index > 0 && index < (this.ticks.length - 1)) {
        el.style.left = `${gutter * index}%`;
      }
    });
  }

  setEvents() {
    this.events.inputHandler = this.handleEvent('change', {
      onElement: this.input,
      withCallback: e => {
        this.flickity.select(e.target.value);
      },
    });

    this.ticks.forEach((tick, index) => {
      this.events[`control${index}`] = this.handleEvent('click', {
        onElement: tick,
        withCallback: e => {
          const n = Array.prototype.indexOf.call(this.ticks, e.currentTarget);
          this.flickity.select(n - 1);
        },
      });
    });
  }
}
