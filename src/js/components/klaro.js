/* eslint-disable func-names */
import * as Klaro from 'klaro/dist/klaro-no-css';
import { resetPageSettingsCookies } from './pageSettings.mjs';

const klaroConfig = {
  storageMethod: 'localStorage',
  storageName: 'eumConsents',
  acceptAll: true,
  translations: {
    zz: {
      privacyPolicyUrl: '/privacy-e-cookies.html',

    },
    en: {
      privacyPolicyUrl: '/',
      consentModal: {
        description:
          'Here you can see and customize the information that we collect about you. '
          + 'Entries marked as "Example" are just for demonstration purposes and are not '
          + 'really used on this website.',
      },
      purposes: {
        functional: {
          title: 'Session/Navigation',
          description: '',
        },
        technical: {
          title: 'Functionality',
        },
        performance: {
          title: 'Performance',
        },
      },
      accessibility: {
        title: 'Accessibility',
      },
    },
    it: {
      privacyPolicyUrl: '/privacy-e-cookies.html',
      consentNotice: {
        description: 'Benvenuto! Possiamo abilitare dei servizi aggiuntivi per {purposes}? Puoi modificare il tuo consenso in qualsiasi momento cliccando sul link "Gestisci impostazioni cookie" nel footer del sito.',
        changeDescription: 'Ci sono stati dei cambiamenti rispetto alla tua ultima visita, ti preghiamo di rinnovare il tuo consenso',
        learnMore: 'Personalizza',
      },
      contextualConsent: {
        description: 'Possiamo caricare i contenuti esterni forniti da {title}?',
      },
      decline: 'No, grazie',
      ok: 'Accetto',
      consentModal: {
        description: 'Qui puoi visualizzare e personalizzare i servizi che gestiscono dati personali.',
        privacyPolicy: {
          name: 'test privacy policy text',
          text: 'Per maggiori informazioni puoi consultare la nostra {privacyPolicy}.',
        },
      },
      purposes: {
        functional: {
          title: 'Sessione/Navigazione',
          description: '',
        },
        technical: {
          title: 'Funzionalità',
          description: '',
        },
        performance: {
          title: 'Prestazioni',
          description: '',
        },
      },
      acceptAll: 'Accetta tutti',
      acceptSelected: 'Accetta selezionati',
      poweredBy: 'Realizzato con Klaro',
      service: {
        disableAll: {
          title: 'Attiva o disattiva tutti i servizi',
          description: 'Usa questo interruttore per attivare o disattivare tutti i servizi',
        },
        optOut: {
          description: 'Questo servizio è attivo di default ma puoi scegliere di disattivarlo',
        },
        purpose: 'Finalità',
        required: {
          description: 'Questo servizio è sempre attivo',
          title: '(sempre attivo)',
        },
      },
      accessibility: {
        title: 'Accessibilità',
        description: 'Memorizziamo le tue scelte sul colore e le dimensioni del carattere per la visualizzazione del sito',
      },
      consent: {
        title: 'Consensi',
        description: 'Salviamo le tue preferenze sull\'utilizzo dei cookie in modo che tu non debba rinnovare il consenso per ogni pagina visitata',
      },
      'google-analytics': {
        title: 'Google Analytics',
        description: 'Raccogliamo statistiche anonime sulle visite per migliorare i contenuti e i servizi che offriamo.',
      },
      wordpress: {
        title: 'WordPress',
        description: 'WordPress installa cookie di sessione che non contengono dati personali',
      },
      youtube: {
        title: 'Video di YouTube',
        description: 'Mostriamo i video di YouTube in modalità di privacy avanzata: vengono installati solo i cookie tecnici, e solo quando l\'utente avvia la riproduzione',
      },
    },
  },
  services: [
    {
      name: 'wordpress',
      purposes: ['functional'],
      cookies: [/^wordpress_.*$/],
      required: true,
    },
    {
      name: 'accessibility',
      purposes: ['technical'],
      cookies: ['text-size', 'color-theme'],
      callback: resetPageSettingsCookies,
    },
    {
      name: 'consent',
      purposes: ['technical'],
      cookies: [/^eumConsent_.*$/],
      required: true,
    },
    {
      name: 'youtube',
      purposes: ['technical'],
      // cookies: [['VISITOR_INFO1_LIVE', 'YSC'], [/^yt.*$/]], // TODO: cookie list
      required: true,
    },
    {
      name: 'google-analytics',
      purposes: ['performance'],
      cookies: [/^_ga_.*$/],
      onlyOnce: true,
    },
  ],
  /*
  You can define an optional callback function that will be called each time the
  consent state for any given service changes. The consent value will be passed as
  the first parameter to the function (true=consented). The `service` config will
  be passed as the second parameter.
  */
  /*
  callback(consent, service) {
    // Global callback
  }, */
};

export default function initKlaro() {
  window.klaroConfig = klaroConfig;
  window.klaro = Klaro;
  Klaro.setup(window.klaroConfig);
}
