const generateSrcSet = sizes => Object.entries(sizes).map(size => ((size[1].width > size[1].height) ? `${size[1].source_url} ${size[1].width}w,` : '')).join('');

const compileArticleTemplate = (article, isSummary = false, isLarge = false, imgSizes = '') => {
  const thumbnail = isSummary || !article.thumbnail ? ''
    : `<figure class="article__image">
        <a href="${article.link}">
          <img src="${article.thumbnail.media_details.sizes.full.source_url}" srcset="${generateSrcSet(article.thumbnail.media_details.sizes)}" alt="${article.thumbnail.title.rendered}" sizes="${imgSizes}">
        </a>
      </figure>`;

  let excerpt = '';
  if (!isSummary) {
    const excerptText = (article.excerpt.rendered.length) > 180 ? `${article.excerpt.rendered.substr(0, 150)}...` : article.excerpt.rendered;
    excerpt = `<div class="article__text">${excerptText}</div>`;
  }

  const jsDate = new Date(article.date);
  const day = jsDate.getDate().toString().length === 1 ? `0${jsDate.getDate()}` : jsDate.getDate();
  const month = (jsDate.getMonth() + 1).toString().length === 1 ? `0${jsDate.getMonth() + 1}` : jsDate.getMonth() + 1;
  const date = `${day}.${month}.${jsDate.getFullYear()}`;
  let kicker = '';

  if (article.categories.length) {
    const category = window.EUM.categories.filter(
      el => el.id === parseInt(article.categories[0], 10),
    );
    kicker = `<div class="article__kicker">
        <a href="${category[0].link}">${category[0].name.toUpperCase()}</a>
      </div>`;
  }

  return `
    <article class="article ${isSummary ? 'm-bottom--medium' : ''}">
    ${thumbnail}
    <div class="article__meta">
      <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512" class="article__icon icon">
        <path d="m 414.99166,414.42924 c 0,24.99946 -20.26605,45.2655 -45.26551,45.2655 H 142.27382 c -24.99946,0 -45.265483,-20.26604 -45.265483,-45.2655 V 97.570758 c 0,-24.999453 20.266023,-45.265496 45.265483,-45.265496 H 286.439 c 6.00266,0 11.7593,2.384587 16.00385,6.628907 l 105.9199,105.919901 c 4.24432,4.24432 6.62891,10.00119 6.62891,16.00362 z m -45.26551,-4.84205 0.0172,-191.6781 -68.11167,0.0136 c -24.99945,0 -45.26549,-20.26604 -45.26549,-45.2655 l 0.009,-75.086182 H 142.27382 V 409.5872 Z m -17.56844,-236.94361 -50.51629,-50.51628 -0.009,50.52965 z"></path>
      </svg>
      ${kicker}
      <time class="article__date">${date}</time>
    </div>
    <div class="article__title ${isLarge ? 'article__title--large' : ''}">
      <a href="${article.link}">${article.title.rendered}</a>
    </div>
    ${excerpt}
    </article>
  `;
};

export default compileArticleTemplate;
