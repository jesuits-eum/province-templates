/* eslint-disable no-underscore-dangle */
import compileArticleTemplate from './articleTemplate.mjs';

const newsSelector = async () => {
  const store = {};
  const mainEl = document.querySelector('.news-dashboard');
  const selector = mainEl.querySelector('.js-news-selector');
  const articlesZone = mainEl.querySelector('.js-news-articles');
  const firstCol = articlesZone.querySelectorAll('.col-12')[0];
  const secondCol = articlesZone.querySelectorAll('.col-12')[1];
  const firstColInitial = firstCol.innerHTML;
  const secondColInitial = secondCol.innerHTML;

  const JSONLinkEl = document.querySelector('link[rel="https://api.w.org/"]');
  const baseUrl = JSONLinkEl ? JSONLinkEl.getAttribute('href') : 'https://gesuiti.it/wp-json/';

  const compileTemplate = (articles, termID) => {
    firstCol.innerHTML = termID
      ? compileArticleTemplate(articles[0], false, true) : firstColInitial;
    const summaries = termID ? articles.slice(1)
      .map(el => compileArticleTemplate(el, true))
      .join('') : secondColInitial;
    secondCol.innerHTML = summaries;
  };

  selector.addEventListener('change', async e => {
    const termID = e.target.value;

    if (store[termID] || termID === '0') {
      compileTemplate(store[termID], termID);
    } else {
      mainEl.classList.add('is-loading');
      const news = await fetch( `${ baseUrl }wp/v2/posts?per_page=6&categories=${ termID }`)
        .then(res => (res.ok ? res.json() : new Promise().reject(res)));

      if (typeof news !== 'undefined' && news.length > 0 && news[0]._links['wp:featuredmedia']) {
        news[0].thumbnail = await fetch(news[0]._links['wp:featuredmedia'][0].href)
          .then(res => (res.ok ? res.json() : new Promise().reject(res)));
      }

      store[termID] = news;
      compileTemplate(news, termID);
      mainEl.classList.remove('is-loading');
    }
  });
};

export default newsSelector;
