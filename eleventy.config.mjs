import { HtmlBasePlugin } from "@11ty/eleventy";
import { eleventyImageTransformPlugin } from "@11ty/eleventy-img";
import eleventyNavigationPlugin from '@11ty/eleventy-navigation';
import htmlMin from 'html-minifier';
import { DateTime } from "luxon";

export default async function ( eleventyConfig ) {

	const config = {
		// Control which files Eleventy will process
		// e.g.: *.md, *.njk, *.html, *.liquid
		templateFormats: [
			"md",
			"njk",
			"html",
		],

		// Pre-process *.md files with: (default: `liquid`)
		markdownTemplateEngine: "njk",

			// Pre-process *.html files with: (default: `liquid`)
			htmlTemplateEngine: "njk",

				// These are all optional:
				dir: {
					input: "src",          // default: "."
					includes: "_includes",
					output: "public",
				},
		// -----------------------------------------------------------------
		// Optional items:
		// -----------------------------------------------------------------

		// If your site deploys to a subdirectory, change `pathPrefix`.
		// Read more: https://www.11ty.dev/docs/config/#deploy-to-a-subdirectory-with-a-path-prefix

		// When paired with the HTML <base> plugin https://www.11ty.dev/docs/plugins/html-base/
		// it will transform any absolute URLs in your HTML to include this
		// folder name and does **not** affect where things go in the output folder.

		pathPrefix: "/province-templates/",
	};

	// Passthroughs
	eleventyConfig.addPassthroughCopy( "src/img" );
	eleventyConfig.addPassthroughCopy( "src/fonts" );
	eleventyConfig.addPassthroughCopy( "src/lib" );
	eleventyConfig.addPassthroughCopy( "src/css" );

	// Run Eleventy when these files change:
	// https://www.11ty.dev/docs/watch-serve/#add-your-own-watch-targets

	// Watch images for the image pipeline.
	eleventyConfig.addWatchTarget( "src/**/*.{svg,webp,png,jpg,jpeg,gif}" );

	// Layout aliases
	eleventyConfig.addLayoutAlias( "base", "layouts/base.njk" );
	eleventyConfig.addLayoutAlias( "page", "layouts/page.njk" );

	// Plugins
	eleventyConfig.addPlugin( eleventyNavigationPlugin );
	eleventyConfig.addPlugin( HtmlBasePlugin );

	/*
	eleventyConfig.addPlugin( eleventyImageTransformPlugin, {
		// Output formats for each image.
		formats: ["avif", "webp", "auto"],

		// widths: ["auto"],

		failOnError: false,
		htmlOptions: {
			imgAttributes: {
				// e.g. <img loading decoding> assigned on the HTML tag will override these values.
				loading: "lazy",
				decoding: "async",
			}
		},

		sharpOptions: {
			animated: true,
		},
	} );
	*/

	// Filters
	eleventyConfig.addFilter( "formatISODate", ( dateString, format ) => {
			return DateTime.fromISO( dateString, { locale: 'it', zone: 'Europe/Rome' } ).toFormat( format );
		} );

	// Transforms
	eleventyConfig.addTransform( "htmlmin", function ( content ) {
			if ( ( this.page.outputPath || "" ).endsWith( ".html" ) ) {
				let minified = htmlMin.minify( content, {
					useShortDoctype: true,
					removeComments: true,
					collapseWhitespace: true,
					minifyCSS: true
				} );

				return minified;
			}

			// If not an HTML output, return content as-is
			return content;
		} );

	// Shortcodes
	eleventyConfig.addShortcode( "youtube", function ( id ) {
			return `<div class="youtube-player js-youtube-player">
			<img src="https://img.youtube.com/vi/${id}/mqdefault.jpg" loading="lazy">
				<iframe type="text-html" width="640" height="390" data-src="https://www.youtube-nocookie.com/embed/${id}?autoplay=1&enablejsapi=1" frameborder="0"></iframe>
		</div>`;
	} );

	// Dev server
	eleventyConfig.setServerOptions( {
		showAllHosts: true,
	});


	return config;
}